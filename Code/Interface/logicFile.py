import os
import numpy as np
import pandas as pd
import shutil
import cv2
import random
import matplotlib.pyplot as plt
import copy
from ultralytics import YOLO
import imutils
import torch
from segment_anything import sam_model_registry, SamAutomaticMaskGenerator, SamPredictor



#Blur simply an image, increase kernel for more blur
def blur_with_mask(img, mask):
	blur = cv2.blur(img,(90,90),0)
	#out = img.copy()
	out = blur * mask[..., np.newaxis]
	return out

# Blur an image with blocs creations (pixelization)
# xmin and ymin are minimum coordinates of the bounding box (in mask)
# w, h are respetclively width and height of the bounding box
def blur_pixelate(img, mask, xmin, ymin, w,h):
	# automatically determine the size of the blurring kernel based
	# on the spatial dimensions of the input image
	kW = int(w / 3)
	kH = int(h / 3)
	# ensure the width of the kernel is odd
	if kW % 2 == 0:
		kW -= 1
	# ensure the height of the kernel is odd
	if kH % 2 == 0:
		kH -= 1
	# apply a Gaussian blur to the input image using our computed
	# kernel size
	blur = cv2.GaussianBlur(img, (kW, kH), 0)
	out = img.copy()
	out[mask>0] = blur[mask>0]
	xSteps = np.linspace(xmin, xmin+w, 8, dtype="int")
	ySteps = np.linspace(ymin, ymin+h, 8, dtype="int")
	# loop over the blocks in both the x and y direction
	for i in range(1, len(ySteps)):
		for j in range(1, len(xSteps)):
			# compute the starting and ending (x, y)-coordinates
			# for the current block
			startX = xSteps[j - 1]
			startY = ySteps[i - 1]
			endX = xSteps[j]
			endY = ySteps[i]
			# extract the ROI using NumPy array slicing, compute the
			# mean of the ROI, and then draw a rectangle with the
			# mean RGB values over the ROI in the original image
			roi = img[startY:endY, startX:endX]
			(B, G, R) = [int(x) for x in cv2.mean(roi)[:3]]
			cv2.rectangle(img, (startX, startY), (endX, endY),
				(B, G, R), -1)
	# return the pixelated blurred image
	return img

def blur_bbox(img, bbox):
	img = img.astype('uint8')
	mask = np.zeros((img.shape[0],img.shape[1]),dtype=np.uint8) # initialize mask
	mask[int(bbox[1]):int(bbox[3]),int(bbox[0]):int(bbox[2])] = 255 # fill with white pixels
	return blur_pixelate(img, mask, int(bbox[0]), int(bbox[1]), int(bbox[2])-int(bbox[0]), int(bbox[3]) - int(bbox[1]))

# draw mask on image from segmentation with sam
def draw_masks_fromList(image, masks_generated) :
	masked_image = image.copy()
	print("1.3")
	red = (0, 0, 255)
	print("1.2")
	for i in range(len(masks_generated)) :
		masked_image = np.where(np.repeat(masks_generated[i][:, :, np.newaxis], 3, axis=2),
														np.asarray(red, dtype='uint8'),
														masked_image)

		masked_image = masked_image.astype(np.uint8)
	print("1.1")

	return cv2.addWeighted(image, 0.3, masked_image, 0.7, 0)


def detect(yolo, img):
    results = yolo(img)
    boxes = results[0].boxes.xyxy.cpu().numpy()
    red = (0, 0, 255)
    out = img.copy()
    for b in boxes :
        box = b.tolist()
        bbmin = (int(box[0]), int(box[1]))
        bbmax = (int(box[2]), int(box[3]))
        out = cv2.rectangle(img, bbmin, bbmax, red, 5)
    return out

def pixelate(yolo, img):
    results = yolo(img)
    boxes = results[0].boxes.xyxy.cpu().numpy()
    out = img.copy()
    for b in boxes :
        box = b.tolist()
        out = blur_bbox(img, box)
    return out

def segment(yolo, sam, img):
	results = yolo(img)
	print("8")
	sam.set_image(img)
	print("7")
	boxes = np.array(results[0].boxes.data.cpu())
	print("6")
	input_boxes = torch.tensor(boxes[:, :-2], device=sam.device)
	print("5")
	transformed_boxes = sam.transform.apply_boxes_torch(input_boxes, frame.shape[:2])
	print("4")
	masks, scores, _ = sam.predict_torch(
		point_coords=None,
		point_labels=None,
		boxes=transformed_boxes,
		multimask_output=False,
	)
	print("3")
	masks = torch.squeeze(masks, 1)
	print("2")
	out = draw_masks_fromList(img, masks.to('cpu'))
	print("1")
	return out

def segment_blur(yolo, sam, img):
	results = yolo(img)
	sam.set_image(img)
	boxes = np.array(results[0].boxes.data.cpu())
	input_boxes = torch.tensor(boxes[:, :-2], device=sam.device)
	transformed_boxes = sam.transform.apply_boxes_torch(input_boxes, frame.shape[:2])
	masks, scores, _ = sam.predict_torch(
		point_coords=None,
		point_labels=None,
		boxes=transformed_boxes,
		multimask_output=False,
	)

	for segmentation_mask in masks:
		segmentation_mask = segmentation_mask.cpu().numpy()
		# Convert the segmentation mask to a binary mask
		binary_mask = np.where(segmentation_mask > 0.5, 1, 0)
		out = blur_with_mask(img, binary_mask)
	return out


#do at launching because takes some time
sam_checkpoint = "sam_vit_h_4b8939.pth"
model_type = "vit_h"
device =  torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
sam = sam_model_registry[model_type](checkpoint=sam_checkpoint)
sam.to(device=device)
predictor = SamPredictor(sam)

# Using YOLO's custom dataset trained model
model=YOLO('best-fd.pt')

#SOURCE_VIDEO_PATH = 'face.mp4'

def videoTraitement(video_path, i):
	print("Passe la aussi")
	vs = cv2.VideoCapture(video_path) # 0 Camera
	writer = None

	try:   # virer pour cam
		prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() \
			else cv2.CAP_PROP_FRAME_COUNT
		total = int(vs.get(prop))
		print("[INFO] {} total frames in video".format(total))

	except:
		print("An error occurred while trying to determine the total")
		total = -1

	grabbed = True

	acc = 0


	while acc < total: # while true pour cam
		(grabbed, frame) = vs.read()
		if grabbed:
			print('frame n°', acc, 'on', total)

			#make treatment on frame
			if(i==0):
				frame = detect(model, frame)
			elif(i==1):
				frame = pixelate(model, frame)
			elif(i==2):
				frame = segment(model, predictor, frame)
			acc+=1
			if writer is None:
				fourcc = cv2.VideoWriter_fourcc(*"MJPG")
				writer = cv2.VideoWriter('output_video_detect.mp4', fourcc, 30,
				(frame.shape[1], frame.shape[0]), True)

			writer.write(frame)

	if writer is not None :
		writer.release()
	vs.release()