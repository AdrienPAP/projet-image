import os
import numpy as np
import pandas as pd
import shutil
import cv2
import random
import matplotlib.pyplot as plt
import copy
from ultralytics import YOLO
import torch
import imutils
from segment_anything import sam_model_registry, SamPredictor
#from supervision import VideoInfo, get_video_frames_generator, VideoSink

def blur_with_mask(img, mask):
	img = img.astype(int)
	blur = cv2.blur(img,(50,50),0)
	out = img.copy()
	out[mask>0] = blur[mask>0]
	return out


# bs=' ' # blank-space
# class_id=0 # id for face
# newline='\n' # new line character
# extension='.txt' # extension for text file
# curr_path = os.getcwd()
# data_path = '../face-detection-dataset/images'

# Using YOLO's custom dataset trained model
model=YOLO('face-detection-dataset/best-fd.pt')
DEVICE = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

MODEL_TYPE = "vit_h"

sam = sam_model_registry[MODEL_TYPE](checkpoint='face-detection-dataset/sam_vit_h_4b8939.pth')
sam.to(device=DEVICE)

SOURCE_VIDEO_PATH = 'face-detection-dataset/face3.mp4'


# m = random.randint(0,150) #Random image number
# test_image = os.path.join(data_path, os.listdir(data_path)[m])

# res = model(test_image)
# mask_predictor.set_image(res[0].plot())
# index_face = 0
# img = cv2.imread(test_image)
# for b in res[0].boxes:
# 	index_face += 1
# 	box = b.xyxy.cpu().numpy();
# 	masks, scores, logits = mask_predictor.predict(
# 	  box=box,
# 	  multimask_output=True
# 	)
# 	img = blur_with_mask(img, masks[np.argmax(scores)])
# cv2.imwrite('img-res-blurred.jpg', img)

vs = cv2.VideoCapture('face-detection-dataset/face3.mp4')
writer = None

try:
	prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() \
		else cv2.CAP_PROP_FRAME_COUNT
	total = int(vs.get(prop))
	print("[INFO] {} total frames in video".format(total))

except:
	print("An error occurred while trying to determine the total")
	total = -1

	# for frame in generator:
	# 	res = model(frame)
	# 	img = frame.copy()
	# 	mask_predictor.set_image(img)
	# 	for b in res[0].boxes:
	# 		box = b.xyxy.cpu().numpy();
	# 		print(box)
	# 		masks, scores, logits = mask_predictor.predict(
	# 		  box=box,
	# 		  multimask_output=True
	# 		)
	# 		img = blur_with_mask(img, masks[np.argmax(scores)])
	# 	sink.write_frame(img)

grabbed = True

acc = 0
while acc < total :
    (grabbed, frame) = vs.read()

    if grabbed:

        frame = cv2.cvtColor(frame.astype('uint8'), cv2.COLOR_RGB2BGR)
        acc +=1
        if acc % (total//5) == 0:
            print('frame n°', acc, 'on', total)

        # predict bounding boxes on frame
        results = model(frame)

        # predict segment on frame
        mask_predictor = SamPredictor(sam)
        mask_predictor.set_image(frame)

        # draw boudning boxes on frame
        boxes = np.array(results[0].boxes.data.cpu())

        input_boxes = torch.tensor(boxes[:, :-2], device=mask_predictor.device)

        transformed_boxes = mask_predictor.transform.apply_boxes_torch(input_boxes, frame.shape[:2])

        masks, scores, _ = mask_predictor.predict_torch(
            point_coords=None,
            point_labels=None,
            boxes=transformed_boxes,
            multimask_output=True,
        )

        mask = masks[0][np.argmax(scores.to('cpu'))]

        frame = blur_with_mask(frame, mask.to('cpu'))

        # save frame in video writer
        if writer is None:
            fourcc = cv2.VideoWriter_fourcc(*"MJPG")
            writer = cv2.VideoWriter('face-detection-dataset/output_video.mp4', fourcc, 30,
            (frame.shape[1], frame.shape[0]), True)

        frame = cv2.cvtColor(frame.astype('uint8'), cv2.COLOR_RGB2BGR)
    writer.write(frame)
if writer is not None :
	writer.release()